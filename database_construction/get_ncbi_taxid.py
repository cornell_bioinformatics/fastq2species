#!/usr/bin/python3
import re
from ete3 import NCBITaxa, Tree, TreeStyle
ncbi = NCBITaxa()

speciesList = []
with open("barcode_species", 'r') as fhs:
    for line in fhs:
        if (re.search("\w", line)):
            line = line.rstrip()
            data = line.split("\t")
            sp = data[1]
            if sp not in speciesList:
                speciesList.append(sp)
    fhs.close()


name2taxid = ncbi.get_name_translator(speciesList)
taxIdList = []
fhw = open("species_annot.txt", 'w')
fhw.write("Species\tPhylum\tClass\tOrder\tFamily\tSubFamily\tTribe\tGenus\n")
for name in speciesList:
    taxId = name2taxid[name][0]
    taxIdList.append(taxId)
    taxaPhylum = ""
    taxaClass = ""
    taxaOrder = ""
    taxaFamily = ""
    taxaSubFamily = ""
    taxaTribe = ""
    taxaGenus = ""
    

    lineage = ncbi.get_lineage(taxId)
    ranks = ncbi.get_rank(lineage)
    names = ncbi.get_taxid_translator(lineage)    
    for tid in lineage:
        if (ranks[tid] == "phylum"):
            taxaPhylum  = names[tid]
        if (ranks[tid] == "class"):
            taxaClass  = names[tid]
        if (ranks[tid] == "order"):
            taxaOrder  = names[tid]
        if (ranks[tid] == "family"):
            taxaFamily = names[tid]
        if (ranks[tid] == "subfamily"):
            taxaSubFamily = names[tid]
        if (ranks[tid] == "tribe"):
            taxaTribe = names[tid]
        if (ranks[tid] == "genus"):
            taxaGenus = names[tid]
    fhw.write(f"{name}\t{taxId}\t{taxaPhylum}\t{taxaClass}\t{taxaOrder}\t{taxaFamily}\t{taxaSubFamily}\t{taxaTribe}\t{taxaGenus}\n")

            


# [1, 131567, 2759, 33154, 33208, 6072, 33213, 33511, 7711, 89593, 7742,
# 7776, 117570, 117571, 8287, 1338369, 32523, 32524, 40674, 32525, 9347,
# 1437010, 314146, 9443, 376913, 314293, 9526, 314295, 9604, 207598, 9605,
# 9606]







#tree = ncbi.get_topology(taxList)
#tree.write(format=1, outfile="new_tree_fullname.nw", features=["sci_name", "rank"])
#tree.write(format=1, outfile="new_tree.nw")
#circular_style = TreeStyle()
#circular_style.mode = "c" # draw tree in circular mode
#circular_style.scale = 20
#tree.render("tree.png", tree_style=circular_style, features=["sci_name"])
#print(tree.get_ascii(attributes=["sci_name", "rank"]))
#229049