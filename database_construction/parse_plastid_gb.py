#!/usr/bin/python

import sys
gb_file = "\\tmp\\plastid.gb"
fh1 = open ("barcode_species",'w')


## search term (chloroplast or plastid ) AND genome 
## rbcL ndhF matK rpoC1  rpoB
genes = {"rbcL",  "ndhF",  "matK",  "rpoC1",  "rpoB"}

geneToFh = {}
for gene in genes:
    print (gene)
    fh = open (f"{gene}.fasta",'w')
    geneToFh[gene] = fh



from Bio import SeqIO
for gb_record in SeqIO.parse(open(gb_file,"r"), "genbank") :
    name = gb_record.name
    desc  = gb_record.description
    source = gb_record.annotations["source"]
    species = gb_record.annotations["organism"]
    replicate = {}

    for feature in gb_record.features:
        if (feature.type == "CDS"):
            geneName = feature.qualifiers.get("gene")
            if (geneName and (geneName[0] in genes)):
                geneId = geneName[0]
                if (geneId in replicate):
                    continue
                replicate[geneId] = 1
                location = feature.location
                fh1.write (f"{name}\t{species}\t{geneId}\t{location}\n")
                seqObj = feature.extract(gb_record)
                seqObj.id = name + "__" + geneId
                seqObj.description = species
                SeqIO.write(seqObj, geneToFh[geneId], "fasta")
