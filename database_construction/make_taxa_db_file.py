#!/usr/bin/python3
import re
speciesInfo = {}
with open("species_annot.txt", 'r') as fhs:
    for line in fhs:
        if (re.search("\w", line)):
            line = line.rstrip()
            data = line.split("\t")
            speciesInfo[data[0]] = data[1:]
            continue
    fhs.close()

fw = open("genome2species", 'w')

genomeDone = {}
with open("barcode_species", 'r') as fhs:
    for line in fhs:
        if (re.search("\w", line)):
            line = line.rstrip()
            data = line.split("\t")
            if data[0] in genomeDone:
                continue
            genomeDone[data[0]] = 1
            if data[1] in speciesInfo:
                info = "\t".join(speciesInfo[data[1]])
                fw.write(f"{data[0]}\t{data[1]}\t{info}\n")
            else:
                print(data[1])
    fhs.close()
fw.close()
