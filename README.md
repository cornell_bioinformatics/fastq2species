# fastq2species.py

This python script is developed for the PanAnd project (https://www.panand.org/) to detect genus level taxonomy based on whole genome sequencing data. The attached database (plastidDB) is constructed for plants.    
The database is sequences of  plastid genes "rbcL",  "ndhF",  "matK",  "rpoC1",  "rpoB" from NCBI refseq database. After alignment with bwa, the taxa with most matched reads is assigned to the sample.

## Getting Started


### Prerequisites
1. bwa (must in the PATH) https://github.com/lh3/bwa  
2. Python 3.0 or above https://www.python.org/ 
3. Biopython https://biopython.org/  

### Installation
git clone https://bitbucket.org/cornell_bioinformatics/fastq2species.git  
cd fastq2species  
tar xvfz plastidDB.tar.gz  

### Usage 
species.py -i mydata.fastq.gz -o outputFile -s sampleName -d plastidDB -t 8


### Output file
  * a tab-delimited text file with one row. The columns are: Sample, Phylum, Class, Order, Family, Subfamily, Tribe, Genus, Species, Matched Genomes and read counts.


### Other parameters
  * -h	show this help message and exit
  * -i	Input data file. it must be a fastq or fastq.gz file.
  * -d	Database directory. it must be a fastq or fastq.gz file.
  * -s	Sample name.
  * -t	Threads, must be integer. Default 4.



## Authors
* **Qi Sun**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments