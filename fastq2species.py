#!/usr/bin/env python3

import subprocess
import re
import io
import sys
import argparse
from collections import defaultdict
import os
from os.path import isfile

global args
global Mismatch
global AlignedReads
global MaxBestMatches


Mismatch = 0.04
AlignedReads = 1000
MaxBestMatches = 100

def main():

    if sys.version_info[0] < 3:
        raise Exception("This code requires Python 3.")

    parser = argparse.ArgumentParser(description='Detect plant species from fastq file.')
    parser.add_argument('-i','--input',type=str,required=True,help='Input data file. it must be a fastq file.')
    parser.add_argument('-o','--output',type=str,required=True,help='Output file name.')
    parser.add_argument('-d','--db',type=str,required=True, help='DB directory.')
    parser.add_argument('-s','--sample',type=str,required=True,  help='Sample Name.')
    parser.add_argument('-t','--threads',type=int,required=False, default=4, help='Threads, must be an integer.')


    args=parser.parse_args()


    if (not os.path.isfile(f"{args.db}/barcode_genes.fasta")):
        parser.print_usage()
        print(f"Error: The database directory is not right. Cannot find the file {args.db}/barcode_genes.fasta!")
        sys.exit()

    if (not os.path.isfile(f"{args.db}/genome2species")):
        parser.print_usage()
        print(f"Error: The database directory is not right. Cannot find the file {args.db}/genome2species!")
        sys.exit()


    if (not os.path.isfile(args.input)):
        parser.print_usage()
        print(f"Error: Input file {args.input} does not exist!")
        sys.exit()



    cmd = f"bwa aln -t {args.threads} -n {Mismatch} {args.db}/barcode_genes.fasta {args.input} | bwa samse -n {MaxBestMatches} {args.db}/barcode_genes.fasta - {args.input} |samtools view -F 4 - | head -n {AlignedReads}"
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    hits = {} 
    hits = defaultdict(lambda:0,hits)
    for line in io.TextIOWrapper(p.stdout, encoding="utf-8"):
        ttt = line.split("\t")
        if (len(ttt)<10):
            continue
        hit = re.sub(r'__.+', '', ttt[2])
        hits[hit] += 1
        if (len(ttt)>=20 and ttt[19].startswith("XA:Z:")):
            ttt = ttt[19].lstrip("XA:Z:").split(";")
            for a in ttt:
                b = a.split(",")
                if (len(b)>2):
                    hit = re.sub(r'__.+', '', b[0])
                    hits[hit] +=1
    sortedHits = sorted(hits.items(),  key=lambda item: item[1], reverse=True)



    speciesInfo = {}
    with open(f"{args.db}/genome2species", 'r') as fhs:
        for line in fhs:
            if (re.search("\w", line)):
                line = line.rstrip()
                data = line.split("\t")
                speciesInfo[data[0]] = data[1:]
                continue
        fhs.close()



    fw = open(f"{args.output}", 'w') 
    maxCount=5
    Lineage = []
    for i in range(0,20):
        Lineage.append([])

    if (len(sortedHits) < maxCount):
        maxCount = len(sortedHits)

    if (maxCount > 0):
        bestHitCounts  = sortedHits[0][1]
        for i in range(0, maxCount):
            genome =sortedHits[i][0]
            readCount = sortedHits[i][1]
            if (readCount/bestHitCounts<0.8):
                break
            info = speciesInfo[genome]
            sp = info[0]
            if (" " in info[0]):
                sp = re.match("^\\S+ (.+)", sp)[1]

            if sp not in Lineage[0]:
                Lineage[0].append(sp)
            Lineage[19].append(f"{genome}({readCount})")

            for index in range(1, len(info)):
                if info[index] not in Lineage[index]:
                    Lineage[index].append(info[index])

    phylum = ";".join(Lineage[2])
    theclass = ";".join(Lineage[3])
    order = ";".join(Lineage[4])
    family= ";".join(Lineage[5])
    subfamily  = ";".join(Lineage[6])
    tribe  = ";".join(Lineage[7])
    genus  = ";".join(Lineage[8])
    species = ";".join(Lineage[0])
    support = ";".join(Lineage[19])

    fw.write("\t".join([args.sample, phylum, theclass, order, family, subfamily, tribe, genus, species, support]))
    fw.write("\n");





if __name__=="__main__":
    main()

